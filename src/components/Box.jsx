import React from 'react';
import './Box.css';

function Box({ value, onBoxClick }) {
  return (
    <div className="box" onClick={onBoxClick}>
      {value}
    </div>
  );
}

export default Box;