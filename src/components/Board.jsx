import React, { useState } from 'react';
import Box from './Box';
import './Board.css';

function Board() {
  const [xIsNext, setXIsNext] = useState(true);
  const [boxes, setBoxes] = useState(Array(9).fill(null));

  function handleClick(i) {
    if (calculateWinner(boxes) || boxes[i]) {
      return;
    }
    const nextBoxes = boxes.slice();
    if (xIsNext) {
      nextBoxes[i] = 'X';
    } else {
      nextBoxes[i] = 'O';
    }
    setBoxes(nextBoxes);
    setXIsNext(!xIsNext);
  }

  const winner = calculateWinner(boxes);
  let status;
  if (winner) {
    status = 'Winner: ' + winner;
  } else {
    status = 'Next player: ' + (xIsNext ? 'X' : 'O');
  }

  return (
    <>
      <div className="status">{status}</div>
      <div className="board">
        <div className="board-row">
          {renderBoardRow(0, 3, handleClick, boxes)}
        </div>
        <div className="board-row">
          {renderBoardRow(3, 6, handleClick, boxes)}
        </div>
        <div className="board-row">
          {renderBoardRow(6, 9, handleClick, boxes)}
        </div>
      </div>
    </>
  );
}

function renderBoardRow(start, end, onBoxClick, boxes) {
  const row = [];
  for (let i = start; i < end; i++) {
    row.push(<Box key={i} value={boxes[i]} onBoxClick={() => onBoxClick(i)} />);
  }
  return row;
}

function calculateWinner(boxes) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (boxes[a] && boxes[a] === boxes[b] && boxes[a] === boxes[c]) {
      return boxes[a];
    }
  }
  return null;
}

export default Board;
