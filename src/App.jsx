import { useState } from 'react'
import './App.css'
import Board from './components/Board'

function App() {


  return (
    <>
      <h1>Tic Tac Toe Game</h1>
      <Board />
    </>
  )
}

export default App
